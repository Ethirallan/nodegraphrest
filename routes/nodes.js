const express = require('express');
const router = express.Router();

const url = require('url');
const communicator = require('../communicator');
const config = require('../config');
const NodeObject = require('../models/nodeObject');
const TopologyObject = require('../models/topologyObject');

router.post('/nodes/new', async (req, res) => {
    const node = new NodeObject(req.body);
    node.cleanUp();

    var runID = req.body.run_id;

    const topology = new TopologyObject({'run_id': runID});
    topology.cleanUp();

    await communicator.insert(config.table.topology, topology).then((resolve) => {
        node.topology_id = resolve['insertId'];
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    communicator.insert(config.table.nodes, node).then((resolve) => {
        res.status(201);
        res.send({
            message: 'Successfully added new Node'
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/nodes/all/:topology', function(req, res) {
    const topologyID = req.params.topology;
    communicator.getNodes(topologyID).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

// router.delete('/nodes/delete/:id', (req, res) => {
//     const id = req.params.id;

//     communicator.delete(config.table.nodes, id).then((resolve) => {
//         res.status(201);
//         res.send({
//             message:resolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

// router.get('/nodes/get/:id', (req, res) => {
//     const id = req.params.id;
//     communicator.getNodeByID(id).then(function(fromResolve) {
//         res.status(201);
//         res.send({
//             message:fromResolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

module.exports = router;