const express = require('express');
const router = express.Router();
const url = require('url');
const RunObject = require('../models/runObject');
const communicator = require('../communicator');

const config = require('../config');

/**
 * Runs:
 *  /add
 *  /delete/:id
 *  /get/all
 *  /get/:id
 */

router.post('/runs/new', (req, res) => {
    const run = new RunObject(req.body);
    run.cleanUp();

    communicator.insert(config.table.runs, run).then((resolve) => {
        res.status(201);
        res.send({
            id:resolve['insertId']
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});


// router.delete('/runs/delete/:id', (req, res) => {
//     const id = req.params.id;

//     communicator.delete(config.table.nodes, id).then((resolve) => {
//         res.status(201);
//         res.send({
//             message:resolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

router.get('/runs/all', (req, res) => {
    communicator.getRuns().then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/runs/:id', (req, res) => {
    const id = req.params.id;
    communicator.getRunByID(id).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});


router.get('/runs/movie/:id', async (req, res) => {
    const id = req.params.id;
    var topologies = [];
    var frames = [];


    await communicator.getTopologies(id).then(function(resolve) {
        topologies = resolve;
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    frames = await forLoop(topologies);

    res.status(201);
    res.send({
        frames: frames
    });
});

const forLoop = async (topologies) => {
    var nodes1 = [];
    var edges1 = [];
    var nodes2 = [];
    var edges2 = [];
    var frame = [];
    var frames = [];

    for (let index = 0; index < topologies.length; index++) {
        await communicator.getNodes(topologies[index].id).then(function(resolve) {
            nodes2 = resolve;
        }).catch((err) => {
            res.status(500);
            res.send({
                message:err
            });
        });

        await communicator.getEdges(topologies[index].id).then(function(resolve) {
            edges2 = resolve;
        }).catch((err) => {
            res.status(500);
            res.send({
                message:err
            });
        });

        if (nodes1.length > nodes2.length) {
            var nodesCopy = [].concat(nodes1);
            for (let j = 0; j < nodes2.length; j++) {
                for (var i = nodesCopy.length - 1; i >= 0; --i) {
                    if (nodesCopy[i].node_id == nodes2[j].node_id) {
                        nodesCopy.splice(i, 1);
                    }
                }
            }
            frame.push({remove_node: nodesCopy[0]});

        } else if (nodes2.length > nodes1.length) {
            var nodesCopy = [].concat(nodes2);
            for (let j = 0; j < nodes1.length; j++) {
                for (var i = nodesCopy.length - 1; i >= 0; --i) {
                    if (nodesCopy[i].node_id == nodes1[j].node_id) {
                        nodesCopy.splice(i, 1);
                    }
                }
            }
            frame.push({add_node: nodesCopy[0]});
        }

        if (edges1.length > edges2.length) {
            var edgesCopy = [].concat(edges1);
            for (let j = 0; j < edges2.length; j++) {
                for (var i = edgesCopy.length - 1; i >= 0; --i) {
                    if (edgesCopy[i].node1_id == edges2[j].node1_id && edgesCopy[i].node2_id == edges2[j].node2_id) {
                        edgesCopy.splice(i, 1);
                    }
                }
            }
            frame.push({remove_edge: edgesCopy});

        } else if (edges2.length > edges1.length) {
            var edgesCopy = [].concat(edges2);
            for (let j = 0; j < edges1.length; j++) {
                for (var i = edgesCopy.length - 1; i >= 0; --i) {
                    if (edgesCopy[i].node1_id == edges1[j].node1_id && edgesCopy[i].node2_id == edges1[j].node2_id) {
                        edgesCopy.splice(i, 1);
                    }
                }
            }
            frame.push({add_edge: edgesCopy});
        }

        frames.push(frame[0]);

        nodes1 = nodes2;
        nodes2 = [];
        edges1 = edges2;
        edges2 = [];
        frame = [];
    };
    return frames;
}

module.exports = router;