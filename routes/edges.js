const express = require('express');
const router = express.Router();
const url = require('url');
const EdgeObject = require('../models/edgeObject');
const communicator = require('../communicator');
const TopologyObject = require('../models/topologyObject');

const config = require('../config');

router.post('/edges/new', async (req, res) => {
    console.log('Step 0');

    const edge1 = new EdgeObject(req.body);
    edge1.cleanUp();

    console.log('Step 1');

    const edge2 = new EdgeObject({
        node1_id: edge1.node2_id,
        node2_id: edge1.node1_id
    });

    edge2.cleanUp();
    console.log('Step 2');

    var runID = req.body.run_id;
    console.log('Step 3');

    const topology = new TopologyObject({'run_id': runID});
    topology.cleanUp();

    await communicator.insert(config.table.topology, topology).then((resolve) => {
        edge1.topology_id = resolve['insertId'];
        edge2.topology_id = resolve['insertId'];
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    await communicator.insert(config.table.edges, edge1).then((resolve) => {
        console.log('Added Edge 1');
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    communicator.insert(config.table.edges, edge2).then((resolve) => {
        res.status(201);
        res.send({
            message: 'Successfully added new Edge'
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/edges/all/:topology', function(req, res) {
    const topologyID = req.params.topology;
    communicator.getEdges(topologyID).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

// router.delete('/edges/delete/:id', (req, res) => {
//     const id = req.params.id;

//     communicator.delete(config.table.nodes, id).then((resolve) => {
//         res.status(201);
//         res.send({
//             message:resolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

// router.get('/edges/get/:id', (req, res) => {
//     const id = req.params.id;
//     communicator.getEdgeByID(id).then(function(fromResolve) {
//         res.status(201);
//         res.send({
//             message:fromResolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

module.exports = router;