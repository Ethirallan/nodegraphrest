const express = require('express');
const router = express.Router();
const url = require('url');
const config = require('../config');
const MessageObject = require('../models/messageObject');
const communicator = require('../communicator');

/**
 * Messages:
 *  /add
 *  /delete/:id
 *  /get/all/:topology
 *  /get/:id
 */

router.post('/messages/add', (req, res) => {
    var msg = new MessageObject(req.body);
    msg.cleanUp();

    communicator.insert(config.table.message, msg).then((resolve) => {
        res.status(201);
        res.send({
            message: resolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message: err
        });
    });
});

router.delete('/messages/delete/:id', (req, res) => {
    const id = req.params.id;

    communicator.delete(config.table.message, id).then((resolve) => {
        res.status(201);
        res.send({
            message:resolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/messages/get/all/:topology', (req, res) => {
    const topologyID = req.params.topology;
    communicator.getMessages(topologyID).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/messages/get/:id', async (req, res) => {
    const id = req.params.id;


    var msgRes = {

    };

    var topologyID;
    var origin;

    await communicator.getMessageByID(id).then(function(resolve) {
        msgRes.details = resolve[0];
        topologyID = resolve[0].topology_id;
        origin = resolve[0].origin;
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    await communicator.getNodes(topologyID).then(function(resolve) {
        msgRes.nodes = resolve;
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    await communicator.getNodeByID(origin).then(function(resolve) {
        msgRes.origin = resolve[0];
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    await communicator.getEdges(topologyID).then(function(resolve) {
        msgRes.edges = resolve;
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    await communicator.getPath(id).then(function(resolve) {
        msgRes.path = resolve;
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });

    msgRes.edges.forEach(e => {
        msgRes.path.forEach(path => {
            if (e.id == path.edge_id) {
                path.edge_id = e.node1_id + "-" + e.node2_id;
            }
        });
    });

    res.status(201);
    res.send({
        message: msgRes
    });
});

module.exports = router;