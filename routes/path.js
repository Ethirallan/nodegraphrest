const express = require('express');
const router = express.Router();
const url = require('url');
const PathObject = require('../models/pathObject');
const communicator = require('../communicator');

const config = require('../config');

/**
 * Path:
 *  /add
 *  /delete/:id
 *  /get/all
 *  /get/:id
 */

router.post('/path/add', (req, res) => {
    const path = new PathObject(req.body);
    path.cleanUp();

    communicator.insert(config.table.nodes, path).then((resolve) => {
        res.status(201);
        res.send({
            message:resolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});


router.delete('/path/delete/:id', (req, res) => {
    const id = req.params.id;

    communicator.delete(config.table.nodes, id).then((resolve) => {
        res.status(201);
        res.send({
            message:resolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/path/get/all/:message', (req, res) => {
    const messageID = req.params.topology;
    communicator.getPath(messageID).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/path/get/:id', (req, res) => {
    const id = req.params.id;
    communicator.getPathByID(id).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

module.exports = router;