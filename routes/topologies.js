const express = require('express');
const router = express.Router();
const url = require('url');
const TopologyObject = require('../models/topologyObject');
const communicator = require('../communicator');

const config = require('../config');

/**
 * Topologies:
 *  /add
 *  /delete/:id
 *  /get/all/:run
 *  /get/:id
 */

router.post('/topologies/new', (req, res) => {
    const topology = new TopologyObject(req.body);
    topology.cleanUp();

    communicator.insert(config.table.topology, topology).then((resolve) => {
        res.status(201);
        res.send({
            message:resolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

// router.delete('/topologies/delete/:id', (req, res) => {
//     const id = req.params.id;

//     communicator.delete(config.table.nodes, id).then((resolve) => {
//         res.status(201);
//         res.send({
//             message:resolve
//         });
//     }).catch((err) => {
//         res.status(500);
//         res.send({
//             message:err
//         });
//     });
// });

router.get('/topologies/all/:run', (req, res) => {
    const runID = req.params.run;
    communicator.getTopologies(runID).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

router.get('/topologies/get/:id', (req, res) => {
    const id = req.params.id;
    communicator.getTopologyByID(id).then(function(fromResolve) {
        res.status(201);
        res.send({
            message:fromResolve
        });
    }).catch((err) => {
        res.status(500);
        res.send({
            message:err
        });
    });
});

module.exports = router;