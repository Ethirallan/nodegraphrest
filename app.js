const express = require('express');
const config = require('./config');
const bodyParser = require('body-parser');
const cors = require('cors');

const nodes = require('./routes/nodes');
const edges = require('./routes/edges');
const messages = require('./routes/messages');
const topologies = require('./routes/topologies');
const runs = require('./routes/runs');

var app = express();
app.listen(config.db.port);


// middleware
app.use(cors());

//data decoding
app.use(bodyParser.urlencoded({extended: true, limit: "50mb"}));
app.use(bodyParser.json());


// routes

app.get('/', (req, res) => {
    res.send('<h1>Hello World!</h1>');
});

app.use(nodes);
app.use(edges);
app.use(messages);
app.use(topologies);
app.use(runs);

// error handling
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;

    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {

            message: error.message
        }
    });
});


console.log('Running on port: ' + config.db.port);
