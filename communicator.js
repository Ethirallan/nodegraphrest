const mysql = require('mysql');
const config = require('./config');
const object = require('./models/dbObject');

/*------------------------connection---------------------------------- */

var connection = mysql.createConnection({
	host: 		'db',
	user: 		config.db.username,
	password: 	config.db.password,
	database: 	config.db.database
});

console.log("Connecting to database...");
connection.connect(function(error) {
	if ( error ) {
		console.log("Error while connecting to database -> ");
		console.log(error);
	} else {
		console.log("Successful connection to database!");	
	}
});


function querySQL(query){
	console.log(query);

	return new Promise (function( resolve, reject ){
		try{
			connection.query(query, function( err, rows, filed ){
				if ( err ) {
					reject(err);
				} else {
					resolve(rows);
				}
			});
		} catch (error) {
			reject(error);
		}
	});
}

module.exports.getNodes = function (topologyID) {
    const SQLquery = 'SELECT * FROM nodes WHERE topology_id = ' + topologyID;
    return querySQL(SQLquery);
}

module.exports.getNodeByID = function (id) {
    const SQLquery = 'SELECT * FROM nodes WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.getEdges = function (topologyID) {
    const SQLquery = 'SELECT * FROM edges WHERE topology_id = ' + topologyID;
    return querySQL(SQLquery);
}

module.exports.getEdgeByID = function (id) {
    const SQLquery = 'SELECT * FROM edges WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.getRuns = function () {
    const SQLquery = 'SELECT * FROM runs';
    return querySQL(SQLquery);
}

module.exports.getRunByID = function (id) {
    const SQLquery = 'SELECT * FROM runs WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.getTopologies = function (runID) {
    const SQLquery = 'SELECT * FROM topology WHERE run_id = ' + runID;
    return querySQL(SQLquery);
}

module.exports.getTopologyByID = function (id) {
    const SQLquery = 'SELECT * FROM topology WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.getMessages = function (topology_id) {
    const SQLquery = 'SELECT * FROM message WHERE topology_id = ' + topology_id;
    return querySQL(SQLquery);
}

module.exports.getMessageByID = function (id) {
    const SQLquery = 'SELECT * FROM message WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.getPath = function (message_id) {
    const SQLquery = 'SELECT * FROM path WHERE message_id = ' + message_id;
    return querySQL(SQLquery);
}

module.exports.getPathByID = function (id) {
    const SQLquery = 'SELECT * FROM path WHERE id = ' + id;
    return querySQL(SQLquery);
}

module.exports.insert = function(table, object) {
    const SQLquery = 'INSERT INTO ' + table + ' (' + object.listKeys() + ') VALUES (' + object.listValues() + ')';
    return querySQL(SQLquery);
}

module.exports.select = function(table, object) {
    const SQLquery = 'SELECT * FROM ' + table + ' WHERE ' + object.valuesToString();
    return querySQL(SQLquery);
}

module.exports.delete = function(table, id) {
	const SQLquery = 'DELETE FROM ' + table + ' WHERE id = ' + id;
	return querySQL(SQLquery);
}