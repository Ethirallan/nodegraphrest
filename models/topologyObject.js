const DbObject = require('./dbObject');

class TopologyObject extends DbObject {
    constructor(data){
        super(data.id);
        this.run_id = data.run_id;
        this.timestamp = data.timestamp;
    }
}

module.exports = TopologyObject;