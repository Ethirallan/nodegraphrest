const DbObject = require('./dbObject');

class EdgeObject extends DbObject {
    constructor(data){
        super(data.id);
        this.node1_id = data.node1_id;
        this.node2_id = data.node2_id;
        this.topology_id = data.topology_id;
    }
}

module.exports = EdgeObject;