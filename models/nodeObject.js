const DbObject = require('./dbObject');

class NodeObject extends DbObject {
    constructor(data){
        super(data.id);
        this.node_id = data.node_id;
        this.topology_id = data.topology_id;
    }
}

module.exports = NodeObject;
