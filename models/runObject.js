const DbObject = require('./dbObject');

class RunObject extends DbObject {
    constructor(data){
        super(data.id);
        this.name = data.name;
    }
}

module.exports = RunObject;