class DbObject{

    constructor(id){
        this.id = id;
    }    

    isEmpty(){
        var bool = true;
        for(let data in this){
            if(typeof this[data] !== 'undefined'){
                bool = false;
                break;
            }
        }
        return bool;
     }

    valuesToString(){
        var str = "";
        for(let data in this){
            if(typeof this[data] != "undefined"){
                if(str != "") str += ", ";
                if(data != "id") str+= data + " = " + "'" + this[data] + "'";
            }
        }
        return str;
    }
    
    existsValue(value){
        var bool = false;
        for(let data in this){
            if( data == value){
                bool = true;
                break;
            }
        }
        return bool;
    }

    listKeys(){
       var str = "";
       for(let data in this){
           if(typeof this[data]!= "undefined"){
               if(str != "") str += ", " + data ;
               else str += data ;
           }
       }
       return str;
    }
    listValues(){
       var str = "";
       for(let data in this){
           if(typeof this[data]!= "undefined"){
               if(str != "") str += ", \'" + this[data] + "\'";
               else str += "\'" + this[data] + "\'";
           }
       }
       return str;
    }

    cleanUp(){
        var toDel = [];
        for(let data in this) if(typeof this[data] === "undefined") toDel.push(data);
        for(var data of toDel) delete this[data]; 
        return this;
    }
}

module.exports = DbObject;