const DbObject = require('./dbObject');

class MessageObject extends DbObject {
    constructor(data){
        super(data.id);
        this.origin = data.origin;
        this.protocol = data.protocol;
        this.topology_id = data.topology_id;
        this.timestamp = data.timestamp;
    }
}

module.exports = MessageObject;