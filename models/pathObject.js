const DbObject = require('./dbObject');

class PathObject extends DbObject {
    constructor(data){
        super(data.id);
        this.node_id = data.node_id;
        this.edge_id = data.edge_id;
        this.message_id = data.message_id;
        this.timestamp = data.timestamp;
    }
}

module.exports = PathObject;