module.exports.server_url = "sensors.innorenew.eu";
module.exports.api_path = "localhost";

module.exports.db = {
    username: "phpmyadmin",
    password: "kikiriki",
    database: "node_graph",
    port: 3010
}

module.exports.table = {
    nodes: "nodes",
    edges: "edges",
    graph: "graph",
    topology: "topology",
    path: "path",
    message: "message",
    runs: "runs"
}